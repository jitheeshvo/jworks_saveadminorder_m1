/**
 * Extended block used for add save button
 * @category Fsite
 * @subpackage SaveAdminOrder
 * @package Fsite_SaveAdminOrder
 * @author Jitheesh V O <jitheesh@envoydigital.com>
 * @copyright Copyright (c) 2015 Envoy Digital (http://www.envoydigital.com/)
 */
if(AdminOrder){
    AdminOrder.addMethods({
        dataShow: function () {
            if ($('submit_order_top_button')) {
                $('submit_order_top_button').show();
            }
            if ($('save_order_top_button')) {
                $('save_order_top_button').show();
            }
            this.showArea('data');
        },
    });
}
