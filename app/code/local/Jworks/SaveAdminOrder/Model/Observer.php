<?php

/**
 *
 * @category Jworks
 * @subpackage SaveAdminOrder
 * @package Jworks_SaveAdminOrder
 * @author Jitheesh V O <jitheeshvo@gmail.com>
 * @copyright Copyright (c) 2016 Jworks
 */
class Jworks_SaveAdminOrder_Model_Observer
{
    /**
     * Set expire quotes additional fields to filter
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function setExpireQuotesAdditionalFilterFields(Varien_Event_Observer $observer)
    {
        /**
         * @var Mage_Sales_Model_Observer
         */
        $salesObserver = $observer->getEvent()->getSalesObserver();
        $salesObserver->setExpireQuotesAdditionalFilterFields(array('saved_order' => 0));

        return $this;
    }

}
