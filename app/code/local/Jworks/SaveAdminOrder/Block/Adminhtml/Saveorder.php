<?php

/**
 * @category Jworks
 * @subpackage SaveAdminOrder
 * @package Jworks_SaveAdminOrder
 * @author Jitheesh V O <jitheeshvo@gmail.com>
 * @copyright Copyright (c) 2016 Jworks
 */
class Jworks_SaveAdminOrder_Block_Adminhtml_Saveorder extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Saved order page init elements
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_saveorder';
        $this->_blockGroup = 'jworks_saveadminorder';
        $this->_headerText = Mage::helper('jworks_saveadminorder')->__('Manage Saved Orders');
        $this->_addButtonLabel = Mage::helper('jworks_saveadminorder')->__('Add Item');

        $this->_addButton(
            'create_order_top_button', array(
            'label' => Mage::helper('sales')->__('Create New Order'),
            'onclick' => 'setLocation(\'' . $this->getNewOrderUrl() . '\')',
            ), 0, 100, 'header'
        );
        parent::__construct();
        $this->_removeButton('add');
    }

    /**
     * default create new order url
     * @return string
     */
    public function getNewOrderUrl()
    {
        return $this->getUrl('adminhtml/sales_order_create/start');
    }
}