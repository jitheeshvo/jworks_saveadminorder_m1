<?php

/**
 * @category Jworks
 * @subpackage SaveAdminOrder
 * @package Jworks_SaveAdminOrder
 * @author Jitheesh V O <jitheeshvo@gmail.com>
 * @copyright Copyright (c) 2016 Jworks
 */
class Jworks_SaveAdminOrder_Block_Adminhtml_Saveorder_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('saveorderGrid');
        $this->setDefaultSort('saveorder_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceSingleton('sales/quote_collection')
            ->addFieldToFilter('saved_order', true);

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', array(
            'header' => Mage::helper('jworks_saveadminorder')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'entity_id',
        ));

        $this->addColumn('customer_firstname', array(
            'header' => Mage::helper('jworks_saveadminorder')->__('Customer Name'),
            'align' => 'left',
            'index' => 'customer_firstname',
        ));

        $this->addColumn('customer_email', array(
            'header' => Mage::helper('jworks_saveadminorder')->__('Customer Email'),
            'align' => 'left',
            'index' => 'customer_email',
        ));
        $this->addColumn('updated_at', array(
            'header' => Mage::helper('jworks_saveadminorder')->__('Quote saved on'),
            'align' => 'left',
            'index' => 'updated_at',
        ));
        $this->addColumn('base_grand_total', array(
            'header' => Mage::helper('jworks_saveadminorder')->__('G.T. (Base)'),
            'align' => 'left',
            'index' => 'base_grand_total',
            'type' => 'currency',
            'currency' => 'base_currency_code',
        ));

        $this->addColumn('action',
            array(
                'header' => Mage::helper('jworks_saveadminorder')->__('Action'),
                'width' => '100',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('jworks_saveadminorder')->__('Edit'),
                        'url' => array('base' => '*/*/edit'),
                        'field' => 'id'
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
            ));

        $this->addExportType('*/*/exportCsv', Mage::helper('jworks_saveadminorder')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('jworks_saveadminorder')->__('XML'));

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('saveorder_id');
        $this->getMassactionBlock()->setFormFieldName('jworks_saveadminorder');

        $this->getMassactionBlock()->addItem(
            'delete', array(
                'label' => Mage::helper('jworks_saveadminorder')->__('Delete'),
                'url' => $this->getUrl('*/*/massDelete'),
                'confirm' => Mage::helper('jworks_saveadminorder')->__('Are you sure?')
            )
        );
        return $this;
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}