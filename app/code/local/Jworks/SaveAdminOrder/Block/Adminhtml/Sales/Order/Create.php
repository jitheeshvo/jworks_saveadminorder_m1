<?php

/**
 * Extended block used for add save button
 * @category Jworks
 * @subpackage SaveAdminOrder
 * @package Jworks_SaveAdminOrder
 * @author Jitheesh V O <jitheeshvo@gmail.com>
 * @copyright Copyright (c) 2016 Jworks
 */
class Jworks_SaveAdminOrder_Block_Adminhtml_Sales_Order_Create extends Mage_Adminhtml_Block_Sales_Order_Create
{
    /**
     * @Extended for add new save order button
     */
    public function __construct()
    {
        parent::__construct();
        $customerId = $this->_getSession()->getCustomerId();
        $storeId    = $this->_getSession()->getStoreId();
        $this->_addButton(
            'save_order', array(
            'id' => 'save_order_top_button',
            'label' => Mage::helper('sales')->__('Save Order'),
            'onclick' => 'setLocation(\'' . $this->getSaveOrderUrl() . '\')',
            ), 0, 100, 'header'
        );
        if (is_null($customerId) || !$storeId) {
            $this->_updateButton('save_order', 'style', 'display:none');
        }
    }

    /**
     * get custom save order url
     * @return string
     */
    public function getSaveOrderUrl()
    {
        return $this->getUrl('*/saveadminorder_saveorder/save');
    }
}
