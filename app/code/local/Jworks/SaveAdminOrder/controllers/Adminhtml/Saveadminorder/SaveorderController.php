<?php

/**
 * Admin controller for save quote data
 * @category Jworks
 * @subpackage SaveAdminOrder
 * @package Jworks_SaveAdminOrder
 * @author Jitheesh V O <jitheeshvo@gmail.com>
 * @copyright Copyright (c) 2016 Jworks
 */
class Jworks_SaveAdminOrder_Adminhtml_Saveadminorder_SaveorderController extends Mage_Adminhtml_Controller_action
{
    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/saveorder');
    }

    /**
     * return current admin quote session
     * @return Mage_Core_Model_Abstract
     */
    protected function _getSession()
    {
        return Mage::getSingleton('adminhtml/session_quote');
    }

    /**
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('saveorder/items')
            ->_addBreadcrumb(
                Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager')
            );

        return $this;
    }

    /**
     * Display saved order grid page
     */
    public function indexAction()
    {
        $this->_initAction()
            ->renderLayout();
    }

    /**
     * Save order data
     */
    public function saveAction()
    {
        $adminQuote = $this->_getSession()->getQuote();

        if ($adminQuote->getId()) {

            try {

                $adminQuote->setData('saved_order', true)->save();
                Mage::getSingleton('adminhtml/session')
                    ->addSuccess($this->__('Order has been saved. See All saved orders from <a href="%s">here</a>'
                        , $this->getUrl('adminhtml/saveadminorder_saveorder')));
            } catch (Mage_Core_Exception $e) {

                if (!empty($e->getMessage())) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
            } catch (Exception $e) {

                Mage::getSingleton('adminhtml/session')
                    ->addException($e, $this->__('Order save failed: %s', $e->getMessage()));
            }

        }
        $this->_redirect('adminhtml/sales_order/index');

    }

    /**
     * Deleted quote from saved order list
     */
    public function massDeleteAction()
    {
        $quoteIds = $this->getRequest()->getParam('jworks_saveadminorder');

        if (!is_array($quoteIds)) {

            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {

            try {

                $savedOrderCollection = Mage::helper('jworks_saveadminorder')->getSavedOrderCollection();
                $savedOrderCollection->addFieldToFilter('entity_id', array('in' => $quoteIds));

                foreach ($savedOrderCollection as $adminQuote) {
                    $adminQuote->setData('saved_order', false)->save();
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($quoteIds)
                    )
                );

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * Edit saved order data, set quote and move to order create page
     */
    public function editAction()
    {
        $quoteId = $this->getRequest()->getParam('id');

        if ($quoteId) {

            $this->_getSession()->setQuoteId($quoteId);
            $adminQuote = $this->_getSession()->getQuote();

            $this->_redirect(
                'adminhtml/sales_order_create',
                array('quote_id' => $quoteId, 'from' => 'jworks_saveadminorder',
                    'customer_id' => $adminQuote->getCustomerId(), 'store_id' => $adminQuote->getStoreId(),
                    'currency_id' => $adminQuote->getQuoteCurrencyCode())
            );

        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('saveorder')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }


    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'saved_orders.csv';
        $grid = $this->getLayout()->createBlock('jworks_saveadminorder/adminhtml_saveorder_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportXmlAction()
    {
        $fileName = 'saved_orders.xml';
        $grid = $this->getLayout()->createBlock('jworks_saveadminorder/adminhtml_saveorder_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }
}