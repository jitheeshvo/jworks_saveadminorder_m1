<?php

/**
 *
 * @category Jworks
 * @subpackage SaveAdminOrder
 * @package Jworks_SaveAdminOrder
 * @author Jitheesh V O <jitheeshvo@gmail.com>
 * @copyright Copyright (c) 2016 Jworks
 */
class Jworks_SaveAdminOrder_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * get admin saved quote collection
     * @return Mage_Sales_Model_Resource_Quote_Collection
     */
    public function getSavedOrderCollection()
    {
        $collection = Mage::getResourceSingleton('sales/quote_collection')
            ->addFieldToFilter('saved_order', true);

        return $collection;
    }
}
