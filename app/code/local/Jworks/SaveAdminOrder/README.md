Jworks_SaveAdminOrder
================
@author Jitheesh V O <jitheeshvo@gmail.com>
@copyright Copyright (c) 2016 Jworks

Usage:
- Admin can save new orders by using save button from New order create page
- New menu item for view Saved orders
- New grid display for saved admin orders
- Continue checkout process from saved order area
- mass delete action for saved orders


### Installation
------------
1.  For manual installation copy all folders to project root.
2.  Clear cache
3.  Please Logout and login again for adding acl rules

### Files
---------------------
/app/code/local/Jworks/SaveAdminOrder/
/app/design/adminhtml/default/default/layout/jworks/saveorder/
/app/design/adminhtml/default/default/template/jworks/saveorder/
/skin/adminhtml/jworks/saveorder/save-order.js
/app/etc/modules/Jworks_SaveAdminOrder.xml

### Depend modules
------------
* Mage_Sales

###  Testing installation success
----------------------------
1. Test saved quote/order functionality from Admin > Sales > Create new order, In the new order create page, you can see
that one new button called "Save order"
2. Use this button to save current order data
3. Check saved order/quote data from Admin > Sales > Saved Orders grid
4. Edit order data, validate items, payment method, shipping methods and addess fields

### Limitations
----------------------------
1. By using this functionality you are only allowed to edit one order at a time in same session window.