<?php
/**
 * Extended block used for add save button
 * @category Jworks
 * @subpackage SaveAdminOrder
 * @package Jworks_SaveAdminOrder
 * @author Jitheesh V O <jitheeshvo@gmail.com>
 * @copyright Copyright (c) 2016 Jworks
 */

$installer = $this;

$installer->getConnection()
    ->addColumn(
        $this->getTable('sales_flat_quote'), 'saved_order', array(
            'nullable' => true,
            'default' => 0,
            'length' => 5,
            'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
            'comment' => 'Mark saved admin quotes'
        )
    );