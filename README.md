Jworks_SaveAdminOrder
================

Author Jitheesh V O <jitheeshvo@gmail.com>
copyright Copyright (c) 2017 Jworks

Usage:

* Admin can save new orders by using save button from New order create page
* New menu item for view Saved orders
* New grid display for saved admin orders
* Continue checkout process from saved order area
*mass delete action for saved orders


### Installation
------------

*  For manual installation copy all folders to project root.
* Clear cache
*  Please Logout and login again for adding acl rules

### Files
---------------------

/app/code/local/Jworks/SaveAdminOrder/
/app/design/adminhtml/default/default/layout/jworks/saveorder/
/app/design/adminhtml/default/default/template/jworks/saveorder/
/skin/adminhtml/jworks/saveorder/save-order.js
/app/etc/modules/Jworks_SaveAdminOrder.xml

### Depend modules
------------
* Mage_Sales

###  Testing installation success
----------------------------

* Test saved quote/order functionality from Admin > Sales > Create new order, In the new order create page, you can see
that one new button called "Save order"
* Use this button to save current order data
* Check saved order/quote data from Admin > Sales > Saved Orders grid
* Edit order data, validate items, payment method, shipping methods and addess fields

### Limitations
----------------------------

* By using this functionality you are only allowed to edit one order at a time in same session window.
